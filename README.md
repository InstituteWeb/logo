# Institute Web Logo #

The Inkscape source files are located in `src/`. The optimized SVGs are stored in `dist/` directory.

![Institute Web Logo](https://bytebucket.org/InstituteWeb/logo/raw/bd6502f896bc0826cd8f798f5ce40298824c98b0/dist/InstituteWeb-Logo-full.svg)